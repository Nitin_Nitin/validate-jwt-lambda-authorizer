package com.tillster.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestAuthorizerContext {

    private String type;
    private String methodArn;
    private RequestHeader headers;
    private String sourceIp;
    private Identity identity;

    public RequestAuthorizerContext(){}

    public RequestAuthorizerContext(String type, String methodArn, RequestHeader headers) {
        this.type = type;
        this.methodArn = methodArn;
        this.headers = headers;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMethodArn() {
        return methodArn;
    }

    public void setMethodArn(String methodArn) {
        this.methodArn = methodArn;
    }

    public RequestHeader getHeaders() {
        return headers;
    }

    public void setHeaders(RequestHeader headers) {
        this.headers = headers;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public static class RequestHeader {
    	
    	private String xXsrfToken;
    	private String cookie;
    	private String authorization;

        public RequestHeader(){}

        public RequestHeader(String xXsrfToken, String Cookie, String authorization) {
            this.xXsrfToken = xXsrfToken;
            this.cookie = Cookie;
            this.authorization = authorization;
        }

        public String getxXsrfToken() {
			return xXsrfToken;
		}

		public void setxXsrfToken(String xXsrfToken) {
			this.xXsrfToken = xXsrfToken;
		}

		public String getCookie() {
			return cookie;
		}

		public void setCookie(String cookie) {
			this.cookie = cookie;
		}

		public String getAuthorization() {
			return authorization;
		}

		public void setAuthorization(String authorization) {
			this.authorization = authorization;
		}

		@Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    public static class Identity {
        private String sourceIp;

        public String getSourceIp() {
            return sourceIp;
        }

        public void setSourceIp(String sourceIp) {
            this.sourceIp = sourceIp;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
