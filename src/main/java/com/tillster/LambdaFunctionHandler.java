package com.tillster;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.tillster.model.RequestAuthorizerContext;
import com.tillster.util.AuthPolicy;
import com.tillster.util.TokenValidator;

/**
 * Handles IO for a Java Lambda function as a custom authorizer for API Gateway
 */
public class LambdaFunctionHandler implements RequestStreamHandler {

	private final static ObjectMapper objectMapper = new ObjectMapper();

	public static final String XSRF_TOKEN = "XSRF-TOKEN";
	public static final String COOKIE_CANT_BE_EMPTY_MESSAGE = "Cookies cannot be empty";
	public static final String XSRF_CANT_BE_EMPTY_MESSAGE = "XSRF header cannot be empty";
	public static final String XSRFCOOKIE_CANT_BE_EMPTY_MESSAGE = "XSRF Cookie header cannot be empty";
	public static final String COOKIE_SESSION_NOTFOUND_MESSAGE = "Invalid CSRF token found in either header or cookie.";

	@Override
	public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

		LambdaLogger logger = context.getLogger();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

		Map<String, Object> jsonMap = objectMapper.readValue(inputStream, Map.class);
		Map<String, Object> headersMap = (Map<String, Object>) jsonMap.get("headers");
		headersMap.put("xXsrfToken", headersMap.get("X-XSRF-TOKEN") != null ? headersMap.get("X-XSRF-TOKEN") : headersMap.get("x-xsrf-token"));
		jsonMap.put("headers", headersMap);

		RequestAuthorizerContext input = objectMapper.convertValue(jsonMap, RequestAuthorizerContext.class);
		logger.log("\nLambda Invoked with Input: " + input);

		String principalId = "xxxx";
		String methodArn = input.getMethodArn();
		String[] arnPartials = methodArn.split(":");
		String region = arnPartials[3];
		String awsAccountId = arnPartials[4];
		String[] apiGatewayArnPartials = arnPartials[5].split("/");
		String restApiId = apiGatewayArnPartials[0];
		String stage = apiGatewayArnPartials[1];
		String httpMethod = apiGatewayArnPartials[2];
		String resource = "";
		for (int i = 3; i < apiGatewayArnPartials.length; i++)
			resource += "/" + apiGatewayArnPartials[i];
		logger.log("\nResource is: " + resource);
		List<String> jwtValidationApiList = new ArrayList<>();
		jwtValidationApiList.add("/mobilem8/placeorder");
		jwtValidationApiList.add("/tenants/gfp-us/session");

		Map<String, String> cookieMap = new HashMap<>();
		Map<String, String> resultContext = new HashMap<>();
		String token = input.getHeaders().getAuthorization();
		AuthPolicy authPolicy= null;
		// Validating CSRF cookie and header authenticity
		try {
			// Validating CSRF cookie and header authenticity
			if (!AuthPolicy.HttpMethod.GET.toString().equalsIgnoreCase(httpMethod)) {
				logger.log("\n\nState Changing API, validating for CSRF cookie.");

				String xXsrfToken = input.getHeaders().getxXsrfToken();
				logger.log("\n\nX-XSRF-TOKEN Header: " + xXsrfToken);
				String cookies = input.getHeaders().getCookie();
				logger.log("\n\ncookies: " + cookies);

				Optional.ofNullable(xXsrfToken)
						.orElseThrow(() -> new IllegalArgumentException(XSRF_CANT_BE_EMPTY_MESSAGE));
				Optional.ofNullable(cookies)
						.orElseThrow(() -> new IllegalArgumentException(COOKIE_CANT_BE_EMPTY_MESSAGE));
				Arrays.stream(cookies.split(","))
						.forEach(cookie -> cookieMap.put(cookie.split("=")[0], cookie.split("=")[1]));

				String xsrfCookie = Optional.ofNullable(cookieMap.get(XSRF_TOKEN))
						.orElseThrow(() -> new IllegalArgumentException(XSRFCOOKIE_CANT_BE_EMPTY_MESSAGE));
				logger.log("\n\nXSRF-TOKEN in Cookie: " + xsrfCookie);

				if (xXsrfToken.equalsIgnoreCase(xsrfCookie))
					logger.log("\n\nCSRF cookie and Header matched");
				else
					throw new IllegalArgumentException(COOKIE_SESSION_NOTFOUND_MESSAGE);
			}
			// Validating JWT Token
			if (jwtValidationApiList.contains(resource)) {
				String isValidToken = TokenValidator.validateToken(token);
				if (!TokenValidator.VALID_TOKEN.equals(isValidToken))
					throw new IllegalAccessException(isValidToken);
				logger.log("\n\nToken Validated\n\n");
				resultContext.put("message", TokenValidator.VALID_TOKEN);
				authPolicy = new AuthPolicy(principalId,
						AuthPolicy.PolicyDocument.getAllowAllPolicy(region, awsAccountId, restApiId, stage),
						resultContext);
				objectMapper.writeValue(outputStream, authPolicy);
				return;
			}
		} catch (IllegalArgumentException e) {
			logger.log("\n\nIllegal Argument Exception Occurred: " + e.getLocalizedMessage() + "\n\n");
			//resultContext.put("message", COOKIE_SESSION_NOTFOUND_MESSAGE);
			resultContext.put("message", e.getLocalizedMessage());
			authPolicy = new AuthPolicy(principalId,
					AuthPolicy.PolicyDocument.getDenyAllPolicy(region, awsAccountId, restApiId, stage), resultContext);
			objectMapper.writeValue(outputStream, authPolicy);
			return;
		} catch (IllegalAccessException e) {
			logger.log("\n\nInvalid token Exception Occurred: " + e.getLocalizedMessage() + "\n\n");
			resultContext.put("message", e.getLocalizedMessage());
			authPolicy = new AuthPolicy(principalId,
					AuthPolicy.PolicyDocument.getDenyAllPolicy(region, awsAccountId, restApiId, stage), resultContext);
			objectMapper.writeValue(outputStream, authPolicy);
			return;
		} catch (ParseException | JOSEException e) {
			logger.log("\n\nToken decode failure: " + e.getLocalizedMessage() + "\n\n");
			resultContext.put("message", "Token decode failure: " + e.getLocalizedMessage());
			authPolicy = new AuthPolicy(principalId,
					AuthPolicy.PolicyDocument.getDenyAllPolicy(region, awsAccountId, restApiId, stage), resultContext);
			objectMapper.writeValue(outputStream, authPolicy);
			return;
		} catch (Exception e) {
			logger.log("\n\nException Occurred: " + e.toString() + "\n\n");
			resultContext.put("message", e.getLocalizedMessage());
			authPolicy = new AuthPolicy(principalId,
					AuthPolicy.PolicyDocument.getDenyAllPolicy(region, awsAccountId, restApiId, stage), resultContext);
			objectMapper.writeValue(outputStream, authPolicy);
			return;
		}
		authPolicy = new AuthPolicy(principalId,
				AuthPolicy.PolicyDocument.getAllowAllPolicy(region, awsAccountId, restApiId, stage), resultContext);
		objectMapper.writeValue(outputStream, authPolicy);

	}
}
